#! /usr/bin/env bash

# Operate only in the home directory
cd ~

# Clone dotfiles
if test ~/.dotfiles; then
	echo "Dotfiles already installed! Run 'config status' to check their status."
else
	echo "Installing dotfiles to '$HOME'..."
	cd ~
	git clone --bare --recurse-submodules https://git.average.name/AverageHelper/dotfiles.git
	echo "Cloned dotfiles repo! Moving 'dotfiles.git' to '.dotfiles'..."
	mv ~/dotfiles.git ~/.dotfiles
	echo "Moved!"
fi

# Make sure 'config' alias is set correctly
__EXPECTED_CONFIG_ALIAS="config='git --git-dir=\"\$HOME/.dotfiles/\" --work-tree=\"\$HOME\"'"

# If the alias is set to something unexpected, yell about it!
if [[ "$(alias | grep 'config')" != "" && "$(alias | grep 'config')" != "$__EXPECTED_CONFIG_ALIAS" ]]; then
	echo
	echo "======================================="
	echo "WARNING: 'config' is already aliased: $(alias | grep 'config')"
	echo "You may need to fix this manually."
	echo "======================================="
	echo
fi

# Don't bother showing the entire home directory in git status
git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME" config status.showUntrackedFiles no

# Set up completions for `config` command, if supported
echo
if [ type completes &> /dev/null ]; then
	complete -F _complete_alias config
	echo "Completions set for 'config' command!"
else
	echo "Could not set up completions for 'config' command. You're on your own to figure those out."
fi

# If we're on macOS, alias VSCodium configs to the correct path
echo
if [ -d ~/Library/Application\ Support ]; then
	echo "Found '~/Library/Application Support', so we're probably on macOS. Symlinking VSCodium settings..."
	ln -sf ~/.config/VSCodium/User/keybindings.json ~/Library/Application\ Support/VSCodium/User/keybindings.json
	ln -sf ~/.config/VSCodium/User/settings.json ~/Library/Application\ Support/VSCodium/User/settings.json
	echo "Symlinked! (Linux doesn't usually need this, because VSCodium already reads from the '~/.config' folder there.)"
else
	echo "Linux doesn't need to symlink VSCodium configs."
fi

echo
echo "Finished!"
echo "Run 'config status' to see available dotfiles, and follow the prompts to install them, replacing 'git' with 'config'."
echo "You may need to run 'alias $__EXPECTED_CONFIG_ALIAS' to proceed."

echo
echo "To install VSCodium extensions, run 'curl -s https://git.average.name/AverageHelper/dotfiles/raw/branch/main/.vscodium_setup.sh | bash'"

