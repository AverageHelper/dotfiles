# dotfiles

> This repo is a work in progress. Most of my main working thingstuffs are not yet present.

Following suggestions from https://wiki.archlinux.org/title/Dotfiles, with inspiration from several of the repos mentioned there, including [pablox-cl](https://github.com/pablox-cl/dotfiles) and [maximbaz](https://github.com/maximbaz/dotfiles).

Using a bare git repository in the home directory, I add files to be tracked. Host-specific configs can go in different branches.

## Platforms

OS: MacOS, Linux

Shell: Zsh, Bash

Terminal: [kitty](https://sw.kovidgoyal.net/kitty/)

Editors: Micro, Vim, Neovim, VSCodium

Package manager: [Nix](https://nix.dev/install-nix)

## Setup and Usage

To install, simply run the following:

```sh
curl -s https://git.average.name/AverageHelper/dotfiles/raw/branch/main/install.sh | bash
```

You may also try installing manually this way:

```sh
cd ~
git clone --bare --recurse-submodules https://git.average.name/AverageHelper/dotfiles.git
mv ~/dotfiles.git ~/.dotfiles
alias config='git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
config config status.showUntrackedFiles no
complete -F _complete_alias config
```

`config` is now `git` for your dotfiles, and accessible anywhere on your system!

Use `config status` and related commands to selectively pull or restore files and changes missing from your current setup. Use `config branch` if this setup needs to be different from `main`.

Usage examples:

```sh
config status
config add .vimrc
config commit -m "Add Vim config"
config add .config/nvim/init.vim
config commit -m "Add NeoVim config"
config push
```

## Initial Setup

For initial setup to create your own bare dotfiles repo, run the following from the home directory:

```sh
git init --bare ~/.dotfiles
alias config='git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
config config status.showUntrackedFiles no
complete -F _complete_alias config
config remote add origin https://git.average.name/AverageHelper/dotfiles
config push -u origin main
```

At this point, there now exists a folder called `.dotfiles` that holds the git data for the home directory.

The `config` command aliases git commands for the home folder folder.

## Pull Submodules

Sometimes, an update to the dotfiles repo may include new submodules. Install those with the following:

```sh
config submodule update --init --remote --recursive
```

## `.config`

Config files for common software.

## `.ssh`

Contains the SSH config file.

## `.bashrc` and `.bprofile`

Contains Bash configurations, including custom prompt and useful aliases.

## `.vimrc`

Contains a very basic Vim config. NeoVim looks in `.config/nvim` first, so this file applies specifically to Vim.

## `.vscodium_extensions`

Contains a list of VSCodium extensions I like to install on my systems. Generate with:

```sh
codium --list-extensions > ~/.vscodium_extensions
```

## `.vscodium_setup.sh`

Script to install VSCodium extensions, as listed in [`.vscodium_extensions`](.vscodium_extensions).

## `.zshrc`

Contains Zsh configurations, including custom prompt and useful aliases.

## `girl`

`cat girl` for _nyaa\~_

Disclaimer: This silly meme does not have any actual relation to the behaviors of cats or girls. This silly meme is not to be taken seriously.

## `install.sh`

Clones dotfiles and moves them into place. You may run this script again to refresh symlinks.

## `shell.nix`

Contains Nix packages to install whenever the [`nix-shell`](https://nixos.org) command is run from the home directory.

