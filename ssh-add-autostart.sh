#! /usr/bin/env bash
# Set this file as a login script in KDE to always have the SSH keys loaded

ssh-add ~/.ssh/id_ed25519 </dev/null
