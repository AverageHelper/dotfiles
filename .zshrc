# eval "nix-shell"

# Platform-specific stuff
case `uname` in
	Darwin)
		# commands for macOS go here
		export PATH="/opt/homebrew/bin:/opt/homebrew/sbin:$PATH:/opt/homebrew/opt/coreutils/libexec/gnubin:/opt/homebrew/bin:/opt/homebrew/opt/node@20/bin:/usr/local/bin:/usr/local/sbin:/usr/local/share/npm/bin:/Library/Java/JavaVirtualMachines/jdk-13.jdk/Contents/Home/bin:/usr/local/opt/openjdk/bin:$HOME/Developer/Tools/apache-maven-3.6.3/bin:$HOME/Developer/Tools/depot_tools:$HOME/.local/bin:/usr/local/mysql/bin:$HOME/Developer/Tools/aspectj1.9/bin:$HOME/Library/Python/3.9/bin"
		if [ -d "$HOME/Developer/Tools/adb-fastboot/platform-tools" ] ; then
			export PATH="$HOME/Developer/Tools/adb-fastboot/platform-tools:$PATH"
		fi
		export LDFLAGS="-L/opt/homebrew/opt/node@20/lib"
		export CPPFLAGS="-I/opt/homebrew/opt/node@20/include"
	;;
	Linux)
		# commands for Linux go here
		export PATH="$PATH:/usr/local/bin:/usr/local/sbin:/usr/local/share/npm/bin:$HOME/.cargo/bin:$HOME/.local/bin"

		# zsh keybinds for Linux go here
		## make ctrl+arrow match alt+arrow behavior
		bindkey '^[[1;5D' 'backward-word'
		bindkey '^[[1;5C' 'forward-word'

		## ctrl+up and alt+up to go to start of line
		bindkey '^[[1;3A' 'beginning-of-line'
		bindkey '^[[1;5A' 'beginning-of-line'

		## ctrl+down and alt+down to go to end of line
		bindkey '^[[1;3B' 'end-of-line'
		bindkey '^[[1;5B' 'end-of-line'

		## make ctrl+backspace and alt+backspace behave
		bindkey '^H' 'backward-delete-word'
		bindkey '^[^?' 'backward-delete-word'

		## make delete key behave as expected (inverse of backspace)
		bindkey '^[[3~' 'delete-char'
		bindkey '^[[3;3~' 'kill-word'
		bindkey '^[[3;5~' 'kill-word'
	;;
	FreeBSD)
		# commands for FreeBSD go here
	;;
esac

#export PATH="$PATH:$(npm bin):$(npm get prefix)/lib/node_modules"

# Prompt and Statusline

# Import Headline prompt theme, customize below
# See https://github.com/Moarram/headline/blob/c12368adfbbaa35e7f21e743d34b59f8db263a95/headline.zsh-theme

source ~/.config/zsh/zsh-headline/headline.zsh-theme

# Colorful username
HEADLINE_STYLE_USER=$bold$cyan
HEADLINE_STYLE_PATH=$bold$green

# Use prompt instead of pre-command hook
HEADLINE_INFO_MODE=prompt

# Pad the prompt line with a space
HEADLINE_PROMPT=' %(#.#.%(!.!.$)) '

# Disable host segment
HEADLINE_HOST_CMD=''

# Don't show overline, but still separate new prompt lines by 1
HEADLINE_LINE_CHAR=' '

# Show current time (does not update until next command)
HEADLINE_DO_CLOCK=true

# Show error status if command fails
HEADLINE_DO_ERR=true

# Show when working tree is clean
# HEADLINE_GIT_CLEAN='✔'
HEADLINE_GIT_CLEAN=''

# Pretty git branch
HEADLINE_BRANCH_PREFIX=' '

# Pretty segment separators
HEADLINE_USER_TO_HOST=' at '
HEADLINE_HOST_TO_PATH=' in '
HEADLINE_PATH_TO_BRANCH=' on '
HEADLINE_PAD_TO_BRANCH=' on '

# Use one char ellipses instead of three
HEADLINE_TRUNC_PREFIX='…'

# Import Agnoster prompt theme, customize below
# See https://github.com/agnoster/agnoster-zsh-theme/blob/6bba672c7812a76defc3efed9b6369eeee2425dc/agnoster.zsh-theme
#
# Overrides Headline's prompt, since I prefer Agnoster here,
# but I also like some of Headline's features, so those stay.

source ~/.config/zsh/zsh-agnoster/agnoster.zsh-theme

# Hack to add newline to end of prompt
prompt_newline() {
	printf "\n ➜";
}

AGNOSTER_PROMPT_SEGMENTS+=prompt_newline

# Hack to remove hostname from prompt. (Duplicated from og prompt_context impl)
prompt_context() {
	local user=`whoami`

	# Fancy username postfix if we're in a `toolbox` container
	# Idea from https://www.reddit.com/r/emacs/comments/o84bg2/
	local container_name=""
	if [[ -a /run/.containerenv ]]; then
		source /run/.containerenv
		# container_name=" ⬢ $name"
		container_name="  $name"
		#container_name=" ! $name"
	fi

	if [[ "$user" != "$DEFAULT_USER" || -n "$SSH_CONNECTION" ]]; then
		prompt_segment $PRIMARY_FG default " %(!.%{%F{yellow}%}.)$user$container_name "
	fi
}

# Use legacy prompt:
# username: ~/foo/bar
# $

#PROMPT=$'\n%{\e[1;36m%}%n: %{\e[1;32m%}%~%{\e[m%}\n $ '

# Aliases
if [ -f ~/.zsh_aliases ]; then
	source ~/.zsh_aliases
fi

# SSH Aliases - a file that contains a `ssh` function
# with keywords representing IP addresses, falling back
# on the canonical one.
if [ -f ~/.ssh_aliases ]; then
	source ~/.ssh_aliases
fi

# Fix SSH shenanigans in Kitty et al
export TERM=xterm-256color

# Set the default editor
export VISUAL=micro
export EDITOR=$VISUAL

# NVM
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# Add deno completions
if [[ ":$FPATH:" != *":$HOME/.zsh/completions:"* ]]; then export FPATH="$HOME/.zsh/completions:$FPATH"; fi
autoload -Uz compinit
compinit
