let
	nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/nixos-23.11";
	pkgs = import nixpkgs { config = {}; overlays = []; };
in

pkgs.mkShell {
	# Add packages here from https://search.nixos.org/
	packages = with pkgs; [
		android-tools # Android dev stuff, includes fastboot
		bun # JavaScriptCore-based JS interpreter
		code-minimap # useful in neovim
		#coreutils # `rm`, etc
		curl # expert-mode web browser
		#dig # get DNS records
		#emacsPackages.pbcopy # copy strings to clipboard
		f3 # test storage integrity
		fastfetch # speedy alternative to neofetch
		ffmpeg # do stuff with media
		fzf # Fuzzy search in command line (see Kitty config)
		git # version control
		htop # process monitor
		hugo # Markdown-based site generator
		#inetutils # basic network tools (`ping`, etc)
		hyfetch # gay alternative to neofetch, can use fastfetch for data
		jq # JSON parser
		#less # read long strings
		libgcc # C compiler
		#man # read docs
		#man-pages # common docs
		#man-pages-posix # common docs
		#nano # text editor
		neovim # text editor
		nmap # useful network tool
		nodejs # V8-based JS interpreter
		#openssh # SSH protocol
		#openssl # basic cryptographic protocols
		#procps # process tools
		rclone # CLI for BackBlaze, etc
		rustup # Rust compiler
		toot # Mastodon client
		vim # text editor
		#which # find where commands come from
		zola # Markdown-based site generator
	];

	USER_GIT_COMMAND = "${pkgs.git}/bin/git";
	GIT_EDITOR = "${pkgs.neovim}/bin/nvim";
	NIX_SHELL_PRESERVE_PROMPT=1;
}
