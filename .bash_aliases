# Aliases

alias please='sudo'
alias ls='ls -aphlFG --color=auto'
alias diff='diff -r'
alias refresh='source ~/.bashrc'
alias reload='refresh'
alias code='codium'
alias config='git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
alias brew='sudo -Hu homebrew brew'
alias sizeof='du -sh'
alias icat='kitten icat'

# Platform-specific aliases
case `uname` in
	Darwin)
		# commands for macOS go here
	;;
	Linux)
		# commands for Linux go here
		alias open='xdg-open'
		alias pbcopy='wl-copy'
		alias pbpaste='wl-paste'
	;;
	FreeBSD)
		# commands for FreeBSD go here
	;;
esac

# List unique file extensions in the current directory, recursively (see https://superuser.com/a/232101)
alias extensions="find . -type f | sed -E 's/.+[\./]([^/\.]+)/\1/' | sort -u"

# only need toolbox to run bare on Silverblue. In nix-shell, fastfetch works directly!
#alias neofetch='if type toolbox 2> /dev/null 1>&2; then toolbox run -c=neofetch hyfetch; podman stop neofetch > /dev/null; else hyfetch; fi'
alias neofetch='hyfetch'

# See https://git.average.name/AverageHelper/ograph-rs/
alias og='ograph'

# only need to symlink codium if we can't install it to PATH, like on Silverblue
if ! command -v codium &> /dev/null; then
	alias codium='flatpak run com.vscodium.codium '
fi

# List all processes that are using port 9000
# $ port <number>
function port() {
	sudo lsof -i -P | grep LISTEN | grep ':'"$1"
}

# Resolve AT Proto handles
# $ did <username>
function did() {
	local handle=${1:?'Please specify an AT Proto handle.'}
	local url='https://average.name/xrpc/com.atproto.identity.resolveHandle?handle='"$handle"
	curl -s $url | jq .did | tr -d '"'
}

# Moves the given file to the trash
# $ trash <path>
function trash() {
	local path=${1:?'Please specify a file or directory to trash.'}
	/usr/bin/kioclient5 move "$path" trash:/
}
