# VSCodium configuration

These files are used as-is on Linux platforms. On macOS, the [install script](../../../README.md#setup-and-usage) will create a symlink to these files in the appropriate place.

[VSCodium plugins](../../../.vscodium_extensions) are listed separately, and may be installed by running:

```sh
curl -s https://git.average.name/AverageHelper/dotfiles/raw/branch/main/.vscodium_setup.sh | bash
```

## `keybindings.json`

Custom keybindings.

## `settings.json`

Custom settings.

