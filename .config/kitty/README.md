# Kitty configuration

## `kitty-themes`

A Git submodule pointing to a repository of Kitty themes.

Pull locally using `config submodule update --init --remote`.

## `kitty.conf`

My Kitty config.

## `kitty.conf.default`

The default Kitty config, from https://sw.kovidgoyal.net/kitty/_downloads/433dadebd0bf504f8b008985378086ce/kitty.conf

See also https://sw.kovidgoyal.net/kitty/conf/#sample-kitty-conf

## `theme.conf`

A symlink to a theme config in `kitty-themes/`.

