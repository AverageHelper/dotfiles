# NeoVim configuration

## `init.vim`

My NeoVim config file:
- Auto-installs [`vim-plug`](https://github.com/junegunn/vim-plug) on launch if not present.
- Lists several Vim and NeoVim plugins. Install using `:PlugInstall` and `:UpdateRemotePlugins`.
- Configures the editor and plugins appropriately.

NeoVim will likely yell at you until plugins and related dependencies are installed. Remember to install plugins as described above, and run `:checkhealth` to ensure everything plays nicely.

