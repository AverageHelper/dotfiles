# Config files

## `VSCodium`

Configs for [VSCodium](https://vscodium.com).

## `kitty`

Configs for [Kitty](https://sw.kovidgoyal.net/kitty) terminal emulator.

## `nvim`

Configs for [NeoVim](https://neovim.io).

## `zsh`

Themes for Zsh shell.

## `hyfetch.json`

Config for [hyfetch](https://github.com/hykilpikonna/hyfetch):
- Sets logo color preset.
- Sets data backend to [fastfetch](https://github.com/fastfetch-cli/fastfetch).

