#! /usr/bin/env bash

FILENAME=~/.vscodium_extensions

if ! [ -f "$FILENAME" ]; then
	echo "$FILENAME: No such file or directory"
	exit 1
fi

# Make sure we grab our aliases
shopt -s expand_aliases
if [ -f ~/.bash_aliases ]; then
	source ~/.bash_aliases
fi

if ! command -v codium &> /dev/null; then
	echo "'codium' command not found."
	exit 1
fi

echo "Installing $(sed -n '$=' "$FILENAME") VSCodium extensions..."
echo

# Get list of currently installed extensions
INSTALLED_EXTENSIONS="$(codium --list-extensions)"

# For each line in extensions file,
while read LINE; do
	# If extension is found in installed extension list
	if echo $INSTALLED_EXTENSIONS | grep "$LINE" &> /dev/null; then
		# Already installed
		echo "$LINE is already installed"
	else
		# Not installed yet. Install it
		codium --install-extension "$LINE"
		echo
	fi
done < "$FILENAME"

echo "Finished!"

