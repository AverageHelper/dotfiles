# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH
export PATH="/opt/homebrew/bin:/opt/homebrew/sbin:/opt/homebrew/opt/node@20/bin:$HOME/.cargo/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/node@20/lib"
export CPPFLAGS="-I/opt/homebrew/opt/node@20/include"

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# Get prompt and other customizations
if [ -f ~/.bprofile ]; then
        . ~/.bprofile
fi

# Load nvm, if present
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# Fix SSH shenanigans in Kitty et al
export TERM=xterm-256color

# Set the default editor
export VISUAL=micro
export EDITOR=$VISUAL

# Load Cargo env if it exists
test -x "$HOME/.cargo/env"

# Load completions for Deno, if present
[ -s "$HOME/.local/share/bash-completion/completions/deno.bash" ] && source $HOME/.local/share/bash-completion/completions/deno.bash
